import React, { Component } from 'react';
import ToolBar from './Components/Toolbar';
import SideBar from './Components/Sidebar'
import Backdrop from './Backdrop';
class App extends Component {
  render(){
    return (
    <div>
      <ToolBar/>
      <SideBar/>
      <Backdrop/>
    </div>
    )
  }
}

export default App;
