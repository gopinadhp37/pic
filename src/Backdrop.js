import React from 'react';
import './Backdrop.css';
import Router from './Router'
const Backdrop = props => {
   return (
   <div className = "backdrop">
       <Router/>
   </div>
   )
}

export default Backdrop;