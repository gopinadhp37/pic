import React from 'react';
import './Sidebar.css';
import { NavLink } from 'react-router-dom';

const SideBar = props => {
   return <nav className = "sidebar">
       <div className="sidebar-logo">
           <h1>PUBLICITY ITEM CENTER</h1></div>
       <ul>
           <li><NavLink to = '/addsurvey'><i class="plus circle icon"></i> Add Survey</NavLink></li>
           <li><NavLink to = '/managesurvey'><i class="tasks icon"></i> Manage Survey</NavLink></li>
           <li><NavLink to = '/adduser'><i class="user icon"></i> Add User</NavLink></li>
           <li><NavLink to = '/manageuser'><i class="users icon"></i> Manage User</NavLink></li>
       </ul>
    </nav>
}

export default SideBar;