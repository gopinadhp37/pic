import React from 'react';
import { Button, Checkbox, Form, Segment,Input} from 'semantic-ui-react';
import styled from 'styled-components';

const Wrapper = styled.div`
margin-left : 30px;
width: 85%;
margin-top:100px;
margin-right : 20px;
`

const UserInput = (props) => (
<Wrapper>
<div>
 <Segment>
 <Form onSubmit ={props.handleAddUser}>
    <Form.Field>
      <label>First Name</label>
      <Input placeholder='First Name' type="text" name="firstname" 
             value={props.firstname} onChange ={props.handleChange} icon='user circle outline'/>
    </Form.Field>
    <Form.Field>
      <label>Last Name</label>
      <Input placeholder='Last Name' type="text" name="lastname"
             value={props.lastname} onChange ={props.handleChange} icon='pencil alternate'/>
    </Form.Field>
    <Form.Field>
      <label>E-mail</label>
      <Input placeholder='E-mail' type ="text" name="email"
             value={props.email} onChange ={props.handleChange} icon='envelope'/>
    </Form.Field>
    <Form.Field>
      <label>Contact</label>
      <Input  placeholder='Contact' type ="text" name="contact"
              value={props.contact} onChange ={props.handleChange} icon='phone'/>
    </Form.Field>
    <Form.Field>
      <Checkbox label='I agree to the Terms and Conditions' />
    </Form.Field>
    <Button type='submit' color="blue">Add User</Button>
 </Form>
 </Segment>
</div>
</Wrapper>
);

export default UserInput;