import React from 'react';
import styled from 'styled-components';
import { Table,faTrash } from 'semantic-ui-react';
//import FontAwesomeIcon from '@fortawesome/react-fontawesome'
//import { faTrash } from '@fortawesome/fontawesome-free-solid';



const Wrapper = styled.div`
margin-left : 30px;
width: 85%;
margin-top:100px;
margin-right : 20px;
`

const UserList = (props) => (
    <Wrapper>
    <div>
        <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>First Name</Table.HeaderCell>
                        <Table.HeaderCell>Last Name</Table.HeaderCell>
                        <Table.HeaderCell>E-mail</Table.HeaderCell>
                        <Table.HeaderCell>Contact</Table.HeaderCell>
                        <Table.HeaderCell>Options</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                {props.users.map((user, index) =>(
                    <Table.Row key ={index} >
                        <Table.Cell>{user.firstname}</Table.Cell>
                        <Table.Cell>{user.lastname}</Table.Cell>
                        <Table.Cell>{user.email}</Table.Cell>
                        <Table.Cell>{user.contact}</Table.Cell>
                        <Table.Cell>
                        <i class="trash icon" color="red"  style={{ 'cursor': 'pointer' }}
                           onClick={() => { props.handleDeleteUser(user.id)}}></i>
                        </Table.Cell>
                    </Table.Row>
                    ))}
        </Table>
    </div>
    </Wrapper>
);

export default UserList;