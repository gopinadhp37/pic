import React from 'react';
import styled from 'styled-components';
import { Table,faTrash } from 'semantic-ui-react';
//import FontAwesomeIcon from '@fortawesome/react-fontawesome'
//import { faTrash } from '@fortawesome/fontawesome-free-solid';



const Wrapper = styled.div`
margin-left : 30px;
width: 85%;
margin-top:100px;
margin-right : 20px;
`

const SurveyList = (props) => (
    <Wrapper>
    <div>
        <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>First Name</Table.HeaderCell>
                        <Table.HeaderCell>E-mail</Table.HeaderCell>
                        <Table.HeaderCell>Start Date</Table.HeaderCell>
                        <Table.HeaderCell>End Date</Table.HeaderCell>
                        <Table.HeaderCell>Options</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                {props.surveys.map((survey, index) =>(
                    <Table.Row key ={index} >
                        <Table.Cell>{survey.firstname}</Table.Cell>
                        <Table.Cell>{survey.email}</Table.Cell>
                        <Table.Cell>{survey.startdate}</Table.Cell>
                        <Table.Cell>{survey.enddate}</Table.Cell>
                        <Table.Cell>
                        <i class="trash icon" color="red"  style={{ 'cursor': 'pointer' }}
                           onClick={() => { props.handleDeleteSurvey(survey.id)}}></i>
                        </Table.Cell>
                    </Table.Row>
                    ))}
        </Table>
    </div>
    </Wrapper>
);

export default SurveyList;