import React from 'react';
import { Button, Checkbox, Form, Segment, Input} from 'semantic-ui-react';
import styled from 'styled-components';

const Wrapper = styled.div`
margin-left : 30px;
width: 85%;
margin-top:100px;
margin-right : 20px;
`

const SurveyInput = (props) => (
<Wrapper>
<div>
 <Segment>
 <Form onSubmit ={props.handleAddSurvey}>
    <Form.Field>
      <label>First Name </label>
      <Input placeholder='First Name' type="text" name="firstname" 
             value={props.firstname} onChange ={props.handleChange} icon='user circle'/>
    </Form.Field>
    <Form.Field>
      <label>E-mail</label>
      <Input placeholder='E-mail' type="text" name="email"
             value={props.email} onChange ={props.handleChange} icon='envelope'/>
    </Form.Field>
    <Form.Field>
      <label>Start Date</label>
      <Input type ="date" name="startdate"
             value={props.startdate} onChange ={props.handleChange} icon='calendar alternate'/>
    </Form.Field>
    <Form.Field>
      <label>End Date</label>
      <Input  type ="date" name="enddate"
              value={props.enddate} onChange ={props.handleChange} icon='calendar check'/>
    </Form.Field>
    <Form.Field>
      <Checkbox label='I agree to the Terms and Conditions' />
    </Form.Field>
    <Button type='submit' color="blue">Add Survey</Button>
 </Form>
 </Segment>
</div>
</Wrapper>
);

export default SurveyInput;