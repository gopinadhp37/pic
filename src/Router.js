import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AddSurvey from './pages/AddSurvey';
import ManageSurvey from './pages/ManageSurvey';
import AddUser from './pages/AddUser';
import ManageUser from './pages/ManageUser';


const Router = () => {
    return(
    <Switch>
        <Route exact path ='/addsurvey' component = { AddSurvey }/>
        <Route exact path ='/managesurvey' component = { ManageSurvey }/>
        <Route exact path ='/adduser' component = {AddUser }/>
        <Route exact path ='/manageuser' component = { ManageUser }/>
    </Switch>
    )
}

export default Router;
