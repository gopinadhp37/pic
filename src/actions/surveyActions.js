import {
    GET_SURVEYS,
    ADD_SURVEY,
    DELETE_SURVEY
} from "../constants";

export function getSurveys(dispatch) {
    return function (dispatch) {
        console.log('Action calling')
        dispatch({
            'type': GET_SURVEYS,
            'payload': ''
        })
    }
}

export function addSurvey(survey) {
    return function (dispatch) {
        dispatch({
            'type': ADD_SURVEY,
            'payload': survey
        })
    }
}

export function deleteSurvey(surveyId) {
    return function (dispatch) {
        dispatch({
            'type': DELETE_SURVEY,
            'payload': surveyId
        })
    }
}

