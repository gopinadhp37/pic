import {
    GET_USERS,
    ADD_USER,
    DELETE_USER
} from "../constants";

export function getUsers(dispatch) {
    return function (dispatch) {
        console.log('Action calling')
        dispatch({
            'type': GET_USERS,
            'payload': ''
        })
    }
}

export function addUser(user) {
    return function (dispatch) {
        dispatch({
            'type': ADD_USER,
            'payload': user
        })
    }
}

export function deleteUser(userId) {
    return function (dispatch) {
        dispatch({
            'type': DELETE_USER,
            'payload': userId
        })
    }
}

