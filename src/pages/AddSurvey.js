import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SurveyInput from '../Components/input';
import {
     addSurvey
} from '../actions/surveyActions';

class AddSurvey extends Component {
    
    state = {
        id : Math.random(),
        firstname :'',
        email :'',
        startdate :'',
        enddate :''
    }
    handleChange = (evt) => {
        const value = evt.target.value;
        this.setState({
            ...this.state,
          [evt.target.name]: value
        });
    }
    handleAddSurvey = () => {
        this.props.addSurvey(this.state);
    }
    render(){
        return(
            <div>
                <SurveyInput handleChange = {this.handleChange}
                             handleAddSurvey= { this.handleAddSurvey }/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        surveys: state.surveys.surveys,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addSurvey :addSurvey
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(AddSurvey);
