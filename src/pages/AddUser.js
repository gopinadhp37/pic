import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UserInput from '../Components/UserInput';
import {
     addUser
} from '../actions/userActions';

class AddUser extends Component {
    
    state = {
        id : Math.random(),
        firstname :'',
        lastname :'',
        email :'',
        contact :''
    }
    handleChange = (evt) => {
        const value = evt.target.value;
        this.setState({
            ...this.state,
          [evt.target.name]: value
        });
    }
    handleAddUser = () => {
        
        this.props.addUser(this.state);
    }
    render(){
        return(
            <div>
                <UserInput handleChange = {this.handleChange}
                           handleAddUser= { this.handleAddUser }/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        users: state.users.users,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addUser :addUser
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
