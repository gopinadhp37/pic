import React, { Component } from 'react';
import SurveyList from '../Components/inpuList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    getSurveys,deleteSurvey
} from '../actions/surveyActions';


class ManageSurvey extends Component {
    componentDidMount(){
        this.props.getSurveys();
        console.log(this.props.surveys);
    }
    handleDeleteSurvey = (surveyId) => {
        this.props.deleteSurvey(surveyId);
    }
    render(){
        return(
            <div>
                <SurveyList surveys={ this.props.surveys }
                            handleDeleteSurvey={this.handleDeleteSurvey}/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        surveys: state.surveys.surveys,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getSurveys: getSurveys,
        deleteSurvey: deleteSurvey
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(ManageSurvey);
