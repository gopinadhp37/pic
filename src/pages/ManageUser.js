import React, { Component } from 'react';
import UserList from '../Components/UserList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    getUsers,deleteUser
} from '../actions/userActions';


class ManageUser extends Component {
    componentDidMount(){
        this.props.getUsers();
        console.log(this.props.users);
    }
    handleDeleteUser = (userId) => {
        this.props.deleteUser(userId);
    }
    render(){
        return(
            <div>
                <UserList users={ this.props.users }
                          handleDeleteUser={this.handleDeleteUser}/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        users: state.users.users,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getUsers: getUsers,
        deleteUser: deleteUser
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(ManageUser);
