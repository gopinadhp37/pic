import { combineReducers } from 'redux';
import { surveyReducer } from './surveyReducer';
import { userReducer } from './userReducer'

export default combineReducers({
    surveys: surveyReducer,
    users: userReducer
})
