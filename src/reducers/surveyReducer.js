import{
    GET_SURVEYS,
    ADD_SURVEY,
    DELETE_SURVEY
} from "../constants";

export function surveyReducer(state = {
    surveys: [],
}, action) {
    switch (action.type) {
        case GET_SURVEYS:
            console.log('Action called');
            return {
                ...state,
                surveys: state.surveys
            }
        case ADD_SURVEY:
            return {
                ...state,
                surveys: [...state.surveys, action.payload]
                }
        case DELETE_SURVEY:
            let tempSurveys = [];
            tempSurveys = state.surveys;
            tempSurveys = tempSurveys.filter((survey) => {
            return survey.id !== action.payload
            })
            return {
                ...state,
                surveys: tempSurveys
            }
        default:
            return state;
}
}