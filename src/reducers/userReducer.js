import{
    GET_USERS,
    ADD_USER,
    DELETE_USER
} from "../constants";

export function userReducer(state = { 
    users: [],
}, action) {
    switch (action.type) {
        case GET_USERS:
            console.log('Action called');
            return {
                ...state,
                users: state.users
            }
        case ADD_USER:
            return {
                ...state,
                users: [...state.users, action.payload]
                }
        case DELETE_USER:
            let tempUsers = [];
            tempUsers = state.users;
            tempUsers = tempUsers.filter((user) => {
            return user.id !== action.payload
            })
            return {
                ...state,
                users: tempUsers
            }
        default:
            return state;
}
}